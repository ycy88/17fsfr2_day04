var suits = ["Clubs", "Diamonds", "Hearts", "Spades"]

var numbers = [
  "Ace", 
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "Jack",
  "Queen",
  "King"
];

var deck = [];

for (i in suits) {
  suit = suits[i];
  for (index in numbers) {
    var card = {
      suit: suit,
      rank: numbers[index]
    }
    deck.push(card);
  }
}

console.log("Card: %s", JSON.stringify(deck));

