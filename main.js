const express = require('express');

// create an instance of the application 
var app = express();


// Define routes
app.use(express.static(__dirname + "/public"));

// create server 
// not you cannot put 3000 first, being the 'truthy' value, will always return
// priority is closer to the program
var port = process.argv[2] || 3000;

// bind server to port 3000
app.listen(port, function(){
  console.log('Server is listening on port %s', port);
});
