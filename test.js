
console.log('\nUsing Array object');
var startTime = new Date().getTime();
  for (i = 0; i < 50000; i++) {
    var array = new Array();
      for (i = 0; i < 50000; i++) {
        array.push('bamboozle');
      }
  }
var endTime = new Date().getTime();

console.log('The time taken is %d ms', (endTime-startTime));

console.log('\nUsing Array literal');
var startTime = new Date().getTime();
  for (i = 0; i < 50000; i++) {
    var array = [];
      for (i = 0; i < 50000; i++) {
        array.push('bamboozle');
      }
  }
var endTime = new Date().getTime();

console.log('The time taken is %d ms', (endTime-startTime));